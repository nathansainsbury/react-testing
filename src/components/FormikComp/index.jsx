import React, { Component } from 'react';
import { Formik, Field } from 'formik';
import { Button } from 'reactstrap';
import * as Yup from 'yup';
import axios from 'axios';
import { connect } from 'react-redux';
import TestComp from './../TestComp';

export class FormikComp extends Component{

    constructor(props){
        super(props);
        this.consoleMe = this.consoleMe.bind(this);
        this.state = {
          arr : []
        }
    }

    consoleMe(values){
        this.setState( {
            arr: this.state.arr.concat(values.user_name)
        })
        console.log(this.state)
    }

    render(){
        const list = this.state.arr;
        const listItems = list.map((li, index) =>
        <li key={index}>{li}
        </li>
        );
        const theState = this.state
        return(
            <div id="div">
            <Formik
                theState = {this.state}
                enableReinitialize= {true}
                validateOnChange={false}
                initialValues={ {
                    user_name: ''
                }}
                onSubmit={(values) => {
                    this.consoleMe(values)
                    console.log("submitted")
                }}
                render= {({
                    values,
                    errors,
                    handleSubmit,
                    handleChange,
                    touched,
                    ...props,
                  }) => (
                    <div>
                    <form onSubmit={handleSubmit}>
                    <label>
                        <Field name="user_name" type="text" id="one" onChange={handleChange} value={values.user_name}/>
                        <button
                        id="three"
                        color="info"
                        type="submit"
                        />
                    </label>
                    <label>

                    </label>
                    </form>
                    <p>{values.user_name}</p>
                    <ul>
                    {listItems}
                    </ul>
                    </div>
                  )}
            />
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return state;
}

export default connect(mapStateToProps)(FormikComp)

// validationSchema={Yup.object().shape({
//   user_name: Yup.string()
//     .test(
//       'test1',
//       'not longer than req length',
//       function(value){
//         if(value.length < 4){
//           return false;
//         }
//         return true
//       }
//     )
// })
//
// }
